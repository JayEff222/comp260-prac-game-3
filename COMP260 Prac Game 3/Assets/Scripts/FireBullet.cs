﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {
	public BulletMove bulletPrefab;
	private float reloadTime = 0.0f; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		reloadTime += Time.deltaTime;

		if(Input.GetButtonDown("Fire1") && reloadTime >= 0.3){
			BulletMove bullet = Instantiate (bulletPrefab);
			bullet.transform.position = transform.position;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;

			reloadTime = 0.0f;

		}
	}
}
