﻿using UnityEngine;
using System.Collections;

public class TargetMove : MonoBehaviour {
	private Animator animator;
	public float startTime = 0.0f;

	void Start () {
		//gets the animator component
		animator = GetComponent<Animator> ();
	}
	

	void Update () {
		//set the star par. to true
		//if we passed the start par.
		animator.SetBool("Start", Time.time >= startTime);

	}
	void OnCollisionEnter(Collision collision){
		animator.SetBool ("Hit", true);

	}
	void Destroy(){
		Destroy (gameObject);

	}
}
