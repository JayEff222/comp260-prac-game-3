﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;


	private float bulletLifeTime = 0.0f;
	//public float reloadtime=1.0f;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();

	}
	void FixedUpdate(){
		rigidbody.velocity = speed * direction;

		bulletLifeTime += Time.deltaTime;
		if(bulletLifeTime >= 5.0f){
		OnDestroy ();
		}

	}
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter(Collision collision){
		Destroy (gameObject);

	}
	void OnDestroy(){
		//ParticleSystem explosion = Instantiate (explosionPrefab);
		//explosion.transform.position = transform.position;

		Destroy (rigidbody.gameObject);


	}
}
